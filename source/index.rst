.. Kubernetes documentation master file, created by
   sphinx-quickstart on Tue May  7 14:35:55 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kubernetes's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   00-K8s-Interet-Dev/index.md
   00-Principes/index.md
   01-Installation/index.md
   02-ObjetsSuite/index.md
   03-Application/index.md
   04-Recap/index.md


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
