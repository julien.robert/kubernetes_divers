# Principes

Le nom : kubernetes abbrégé en k8s (le 8 c'est le nombre de lettres en le k et le s)

 ![Schema architecture](architecture_k8s.png)

* Noeud : une machine (physique/VM/...) du cluster
* Controle Plane : un sous ensemble des noeuds (choisis par l'administrateur) et qui ont une responsabilité spécifique : la gestion du cluster
* Les noeuds qui ne sont pas dans le controle plane ont comme responsabilité de faire tourner des conteneurs
* ETCD : base de données clé-valeur distribuée entre les machines du "control plane"
* kube-api-server : sera notre point d'entrée pour parler à kubernetes
* Les autres trucs on s'en fiche pour le moment ..

##  Etcd

En tant qu'utilisateur kubernetes, la seule chose que vous ferez sera de communiquer avec l'API. C'est une bête API REST avec les méthodes GET, PUT, POST, DELETE.
On l'utilise avec l'outil kubectl plutot que curl pour des raisons de comodité !
Ce que fait Kubernetes, c'est maintenir une base de données d'objets que vous allez créer avec cette API.
C'est presque tout. Par ailleurs l'idée des objets de kubernetes est qu'un objet représente un état.
Par exemple, on pourra créer un objet représentant "un conteneur hello-world tourne" :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: hello-world
  namespace: essai
spec:
  containers:
  - name: hello-world
    image: hello-world
```

Si vous créez un tel objet, Kubernetes s'arrangera pour qu'effectivement un tel conteneur tourne tout le temps (si par exemple le conteneur termine, il se relancera).
Remarques que l'objet a des "specs" et deux infos obligatoires de metadonnées : "namespace" et "name".

**Entrailles K8S:**
Ca vous ne le verrez pas, c'est un détail d'implémentation, mais je trouve que ça aide à comprendre : cet objet sera stocké en base de données à la clé "/registry/pods/essai/hello-world" ("/registry/\<TYPE\>/\<NAMESPACE\>/\<NAME\>").

Kubernetes intègre quelques "controleurs" de base qui vont veiller à ce que effectivement, lorsqu'en base de donnée il y a un objet, l'état correspondant est assuré.
En pratique, les controleurs écoutent l'API et réagissent aux évenements : création, modification et suppression d'un objet.

Il est possible de définir ses propres types d'objets et ses propres controleurs (des conteneurs qui ecoutent la création/modification/suppression d'un objet et réagira en fonction pour maintenir un état cohérent avec l'objet). Ca vous n'aurez pas à le faire, mais par contre vous rencontrez et installerez des controleurs tiers.

Ca y est vous savez tout de Kubernetes, et maintenant il va s'agir de :

* installer un environnement permettant de jouer avec k8s
* pratiquer pour être à l'aise avec kubectl en commençant par créer des objets simples
* connaître quels sont les objets kubernetes de base
* définir des états plus complexes
* savoir comment définir une application complexe en définissant tous les objets qui vont bien

### Pour le plaisir

On peut voir les objets kubernetes de ETCD directement avec l'outil etcdctl :

depuis l'hote du controle plane de k8s :

```bash
sudo apt-get update -y
sudo apt-get install -y etcd-client
```

Puis :

```bash
ETCDCTL_API=3 etcdctl --cacert /var/lib/minikube/certs/etcd/ca.crt --cert /var/lib/minikube/certs/etcd/server.crt --key /var/lib/minikube/certs/etcd/server.key --endpoints https://127.0.0.1:2379 get /registry/ --prefix --keys-only
```
