# Récapitulatif et suites à donner

## Récapitulatif

1. Installer Minikube  (cf [](minikube)), lancer un cluster. Vérifier que le cluster tourne en faisant `kubectl get nodes`
2. Ajouter un ingress controler :

   ```bash
   minikube addons enable ingress
   ```

3. Ajouter argocd au cluster (cf [](argocd)). Penser à modifier /etc/hosts pour qu'il soit possible de contacter argocd sur une adresse qui vous convienne :

   ```bash
    sudo bash -c  "echo $(minikube ip) argocd.amoi >> /etc/hosts"
   ```

   **Remarque** : plus tard on pourra ajouter un truc qui fera la résolution dns automatiquement : [Ingress-dns](https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/#Linux) .. Mais plus tard !

4. Se connecter à Argocd <https://argocd.amoi> avec les identifiants obtenus en faisant :

   ```bash
   passw=$(kubectl get secret -n argocd argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode)
   echo Login : admin, password : $passw
   ```

5. Installer son application dans argocd, par exemple hello-flask qui est ici : <https://forgemia.inra.fr/sol_k8s/argocd_examples.git>

![Capture 0 argocd](./argocd_0.png)
![Capture 1 argocd](./argocd_1.png)
![Capture 2 argocd](./argocd_2.png)

## Ce qu'il reste à voir

Des questions avec des réponses brutes, à détailler et expliquer plus en détail !

### Accès aux dépôts privés

**Le problème** : comment faire pour que argocd puisse accéder à nos dépot privés ?
**La solution** : indiquer à argocd un token d'accès aux dépots. Pour cela on créera un objet de type secret (![doc ici](https://argo-cd.readthedocs.io/en/latest/operator-manual/declarative-setup/)) annoté avec un label argocd.argoproj.io/secret-type: repo-creds par exemple :

```yaml
apiVersion: v1
data:
  password: LETOKEN
  type: git
  url: https://forgemia.inra.fr/infosol-sisol/webapps/
  username: jrobert
kind: Secret
metadata:
  labels:
    argocd.argoproj.io/secret-type: repo-creds
  name: private-repo-forgemia-creds
  namespace: argocd
```

### Les secrets

**Le problème** : comment passer des données sensibles à nos conteneurs sans qu'il puisse être possible de voir ces données depuis argocd ?
**La solution** : au travers d'objets de type secret. Ces secrets seront ensuite passés en variable d'environnement aux conteneurs par un mécanisme spécifique.

###  Gestion des données *sensibles*, les sealedSecrets

**Le problème** : comment versionner ces données sensibles ?
**La solution** : sealedsecret est un controleur qui prend en entrée des objets de type sealedsecret qui contiennent les infos cryptées de nos données sensibles et les décode dans des objet de type secret... tout un programme !

### Le CI/CD

**Le problème** : comment garantir que nos applications sont automatiquement déployées lors des commits ?
**La solution** : avec gitlab-ci on génère les images docker puis on modifie nos helm charts pour qu'ils fassent référence aux bonnes versions de nos images. Argocd se charge de faire un pull régulier de nos helm chart.

## La persistence des données

**Le problème** : comment faire avoir des données persistantes dans nos applications ?
**La solution** : Pour tout ce qui peut être en BD, passer par une BD extérieure. Pour les fichiers, on fera des montages NFS dans le conteneur.. tout un programme !

##  Organisation

### Où mettre nos helm charts ?

### Organiser les namespaces / projets

* Nommage des namespaces ?
* Projets : 
  * par application
  * par domaine d'application (GIS / ORE / .. )
  * par environnement (Dev / Recette / Production /...)
  * Un mix des deux (Dev-GIS / Dev-ORE / Recette-GIS / Recette-ORE / .. )

### Qui fait quoi ? Comment documenter ?

**Qui (quel rôle / quel groupe) fait** :

* Adminisration du cluster k8S
* Création de template helm
* Création d'un chart helm pour une application
* Déploiement d'une application dans l'environnement de dev
* Déploiement d'une application dans l'environnement de recette
* Déploiement d'une application dans l'environnement de prod
* Syncrhonisation d'une application dans l'environnement de dev
* Syncrhonisation d'une application dans l'environnement de recette
* Syncrhonisation d'une application dans l'environnement de prod

**Comment documenter ?**

### Infrastructure as code

**Le problème** : Comment garantir que toutes les données sont dans les dépots et qu'on peut récupérer un cluster k8s uniquement à partir de ces dépots

**La solution** : App of apps (cf [dépot app of apps](https://forgemia.inra.fr/sol_k8s/app-of-apps-argocd))

## Cluster K8s de dev et de prod ?

La question se pose d'avoir un cluster pour les applications en dev et celles en prod (et en recette je ne sais pas). 
=> Mon sentiment : un seul cluster suffirait pour tout le monde.

Une question reste d'avoir un cluster de secour avec un dns qui pourrait basculer dessus au besoin.


## DNS Minikube

**Le problème** : comment faire pour ne pas avoir à gérer à la main le dns via /etc/hosts ?
**La solution** Installer le ingress-dns [Ingress-dns](https://minikube.sigs.k8s.io/docs/handbook/addons/ingress-dns/#Linux) .. à tester !
