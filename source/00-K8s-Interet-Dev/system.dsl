workspace {
    model {

        proprietaire = person "Propriétaire d'étude" "Un propriétaire d'étude ou qqn qui le représente" "Utilisateur"
        consommateur = person "Utilisateur" "Un utilisateur qui veut obtenir des informations sur les études" "Utilisateur"


        dw4System = softwareSystem "Donesolweb 4" {
            proxy = container "Proxy" "Gère le routage des requêtes" "Nginx"
            frontApplication = container "Frontend" "Application VueJS" "JS" "frontend" {
                technology = technology "VueJS"
            }
            frontendServer = container "Sert statiquement le code du frontend" 
            backend = container "Backend" "Application Springboot" "Java"
            database = container "Base de données" "Stockage des données" "Postgres"
        }

        forge = softwareSystem "Dépôts Git (forgemia)" {
            frontendRepo = container "Dépôt Git Frontend" "Contient le code source VueJS" "Git"
            backendRepo = container "Dépôt Git Backend" "Contient le code source Springboot" "Git"
            infraRepo = container "Dépôt Git de l'infrastructure" "Git"
        }
        
        proprietaire -> dw4System "Gère les données des études de sol"
        consommateur -> dw4System "Récupère des données d'études de sol"

//        proprietaire -> proxy "va sur https://dw4.gissol.fr" "HTTPS"
//        consommateur -> proxy "va sur https://dw4.gissol.fr" "HTTPS"
        
        proprietaire -> frontApplication "Modifie une étude"

        proxy -> frontendServer "Route les requêtes vers" "/"
        proxy -> backend "Route les requêtes API vers" "/api/"
        frontApplication -> proxy "Appels API" "HTTPS/REST"
        backend -> database "Requêtes" "PostgreSQL"

        frontApplication -> frontendRepo "Versionné" "Git"
        backend -> backendRepo "Versionné" "Git"

        // Déploiement Docker
        dockerEnv = deploymentEnvironment "Docker" {
            deploymentNode "Infrastructure Docker" {
                deploymentNode "Conteneur Proxy" {
                    containerInstance proxy
                }
                deploymentNode "Conteneur Frontend" {
                    containerInstance frontendServer
                }
                deploymentNode "Conteneur Backend" {
                    containerInstance backend
                }
            }
            deploymentNode "Serveur BDD" {
                deploymentNode "Serveur Base de Données" {
                    containerInstance database
                }
        }
        }
    }

    views {
        systemContext dw4System systemContexte {
            include *
            autolayout lr
            title "Contexte DW4"
        }

        container dw4System containerView {
            include *
            autolayout
            title "Diagramme de Conteneurs"
        }

        deployment * dockerEnv vueDocker {
            include *
            autolayout
            title "Diagramme de Déploiement"
        }
      


        styles {
            element "Container" {
                shape roundedbox
            }
            element "Proxy" {
                background "LightGray"
                color "Gray"
            }
            element "frontend" {
                shape WebBrowser
            }
            element "Backend" {
                background "Orange"
                color "DarkRed"
            }
            element "Base de données" {
                background "Yellow"
                color "DarkGreen"
            }
            element "Person" {
                shape Person
            }
        }
    }
}