# Contexte  : Déploiement d'application & besoins des développeurs

## Type de code

SI développés en interne ou par des prestataires, architecture type : multi-niveaux (front/back) avec JAVA (Springboot) et JS (VueJS), une base de données Postgres est hébergée par ailleurs et le développeur ne s'en préoccupe pas.

```{kroki}
   :caption: Contexte système
   :type: structurizr
   :options: { view-key: systemContexte }
   :filename: system.dsl
```

```{kroki}
   :caption: Diagramme de fonctionnement
   :type: structurizr
   :options: { view-key: containerView }
   :filename: system.dsl
```

```{kroki}
   :caption: Diagramme de déploiement docker
   :type: structurizr
   :options: { view-key: vueDocker }
   :filename: system.dsl
```

## Avec docker

* Administration d'une VM "coquille vide" + Gestion des certificats
* Dépôt "deploiement" contenant un docker-compose.yaml

```yaml
version: '3.7'
services:
  reverse_proxy:
    image: haproxy:vXX
    volumes:
       - haproxy.conf:/etc/haproxy.conf # règles proxy vers back/front
       - cert.pem:/etc/ssl/certs/cert.pem #certificat ssl

    ports:
       - "443:443"
  frontend:
    image: registry.forgemia.inra.fr/monappli/front:v2023-05-14

  backend:
    image: registry.forgemia.inra.fr/monappli/back:v2023-05-14
    environment:
      - POSTGRES_SERVER=${POSTGRES_SERVER}
      - POSTGRES_DB=${POSTGRES_DB}
      - POSTGRES_USER=${POSTGRES_USER}
      - POSTGRES_PASSWORD=${POSTGRES_PASSWORD}
```

* Lancement et mise à jour : `docker compose up`.

*Remarque* : les images docker sont soit directement compilées (votre DockerFile est dans le dépot contenant les règles de déploiement) soit déposées sur la forgemia. Le dépot sur la forgemia peut être manuel (docker push), ou intégré au CI avec un Dockerfile dans le dépot de votre code et un gitlabci adapté.

## Avec K8S - En pratique

* Dépôt "déploiement"
* Contient un "helm chart" (équivalent docker-compose, généré semi-automatiquement) :
  * des templates pour chaque objet k8s
  * un fichier values.yaml avec les paramètres

```bash
.
└── charts
    ├── back
    │   ├── templates
    │   │   ├── deployment.yaml # responsable du conteneur back
    │   │   ├── ingress.yaml # règles "proxy" back (monappli.inrae.fr/api/)
    │   │   └── service.yaml
    │   └── values.yaml # paramétrage
    └── front
        ├── templates
        │   ├── deployment.yaml # responsable du conteneur front
        │   ├── ingress.yaml # règles "proxy" front (monappli.inrae.fr/)
        │   └── service.yaml
        └── values.yaml
```

```bash
# Value.yaml du back
replicaCount: 1
image:
  repository: registry.forgemia.inra.fr/monappli/back
  tag: "v2023-05-17"
ingress:
  hosts:
    - host: monappli.inrae.fr
      paths:
        - path: /api/(.*)
```

---

## Démonstration

Une fois ce helm chart déposé sur la forge:

* argocd pour l'installer sur notre cluster (démonstration avec dw4-dev)
* synchronisation automatique
* rollback / suivi basique des logs / debuggage k8s / via argocd

---

## Petit shéma du cluster

![arhchi_k8s.png](./arhchi_k8s.png)


## Intérêt par rapport à Docker

* Gestion des certificats
* Regles de proxy simplifiées
* Outil de suivi argocd
* Intégration CI/CD
* Scalabilité
* 