# Objets : suite

Les objets communs que nous rencontrerons seront :

* pod
* replicaset
* deployment
* service
* ingress
* volume

## Pod

Un objet de type "Pod" représente l'existence d'un conteneur. C'est l'objet le plus "simple" et qui va donner lieu à un ordonnancement immédiat du lancement de conteneur sur un noeud. Lorsqu'on crée un objet de type Pod, l'ordonnanceur de Kubernetes choisit un noeud et lui demande de s'occuper de ce pod.
Grossièrement, cela va se traduire par l'exécution d'une commande telle que "docker run" sur ce noeud via le démon "kubelet".

Par exemple :

```yaml
apiVersion: v1
kind: Pod
metadata:
    name: hello-world
    namespace: default
spec:
    containers:
    - name: hello-world
      image: hello-world
      env:
        - name: HELLO
          value: WORLD
      resources:
        requests:
          memory: "64Mi"
          cpu: "250m"
        limits:
          memory: "128Mi"
          cpu: "500m"    
```

Pour plus de détails, la documentation est ici : <https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.30/#podspec-v1-core>


## ReplicaSet

Un objet de type replicaSet permet de demander l'existence d'un certain nombre de répliques de pods. 
Lorsqu'on crée un objet de type replicaset, le controleur "built in" de Kubernetes va se charger de garantir qu'à tout moment les objets pods existent.
Par exemple :

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata:
    name: hello-world
    namespace: default
spec:
    replicas: 3
    selector:
        matchLabels:
            app: hello-world
    template:
        metadata:
            labels:
                app: hello-world
        spec:
            containers:
            - name: hello-world
              image: hello-world
```

Pour la curiosité, le code source du controleur est ici : <https://github.com/kubernetes/kubernetes/blob/master/pkg/controller/replicaset/replica_set.go>

## Deployment

Un objet de type deployment est un objet kubernetes qui va s'assurer de l'existence d'un ensemble de pods. Par exemple :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
    name: hello-world
    namespace: default
spec:
    replicas: 1
    selector:
        matchLabels:
            app: hello-world
    template:
        metadata:
            labels:
                app: hello-world
        spec:
            containers:
            - name: hello-world
                image: hello-world
```

## Service

Supposons que notre conteneur hello-world serve une page sur son port 8080. Pour des raisons de montée en charge, on demande plusieurs "replicas" de ce conteneur, et on a donc plusieurs pods qui tournent et servent cette page, cahcun ayant son adresse IP. Depuis un pod quelconque du cluser, j'aimerai pouvoir accéder à cette page sans avoir à choisir quel pod contacter précisément. Pour cela, kubernetes donne la possibilité de créer une adresse IP virtuelle, telle que si on la contacte, on sera automatiquement dirigé vers l'un quelconque des pods.

Un objet de type service est un objet kubernetes qui permet de créer un couple (IP, port) au sein du cluster et de relier ce couple à un ensemble de pods sur un targetPort donné. Lorsqu'on contactera cette (IP,port) (par exemple via curl IP:port), on sera automatiquement mis en relation avec l'un quelconque des pods, au hasard, sur le port "targetPort".

```yaml
apiVersion: v1
kind: Service
metadata:
    name: hello-world
    namespace: default
spec:
    type: ClusterIP
    ports:
    - port: 8080
      targetPort: 8080
      protocol: TCP
      name: http
    selector:
        app: hello-world
```

## Ingress

Un objet de type ingress est un objet kubernetes qui va permettre d'exposer un hote http à l'extérieur du cluster et rediriger vers un service donné.

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: hello-world
    namespace: default
spec:
    rules:
    - host: hello-world.info
      http:
        paths:
        - path: /
          pathType: Prefix
          backend:
            service:
                name: hello-world
                port:
                    number: 8080
```
